﻿using System;

namespace Task3Variablesandcontrolstructures
{
    class Program
    {
        static void Main(string[] args)
        {
            #region testingregion

            Console.Write("What is the size of the square");
            int squaresize = Convert.ToInt16(Console.ReadLine());
            bool checkside = true;
            string[,] thesquare = new string[squaresize, squaresize];

            #endregion

            #region Create the sqaure
            for (int i = 0; i < squaresize; i++)
            {
                for (int j = 0; j < squaresize; j++)
                {
                    if (i==0)
                    {
                        checkside = true;
                    }
                    if (j==0)
                    {
                        checkside = true;
                    }
                    if (i== squaresize-1)
                    {
                        checkside = true;
                    }
                    if (j== squaresize-1)
                    {
                        checkside = true;
                    }
                    if (checkside == true)
                    {
                        thesquare [i, j] = "#";
                    }
                    else
                    {
                        thesquare[i, j] = " ";
                    }
                    checkside = false;
                    //Console.WriteLine(thesquare[i, j]);
                }
            }
            #endregion

            #region Printsquare
                                                  
            //Printmatrix
            Console.WriteLine("Printing Matrix: ");
            for (int k = 0; k < squaresize; k++)
            {
                for (int l = 0; l < squaresize; l++)
                {
                    Console.Write(thesquare[k, l]);
                }
                Console.WriteLine();
            }
            Console.ReadLine();
            #endregion
        }
    }
}
