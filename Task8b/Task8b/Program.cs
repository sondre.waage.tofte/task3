﻿using System;

namespace Task8b
{
    class Program
    {
        static void Main(string[] args)
        {
            int squaresize = FindSize();
            string[,] thesquare = Makesquare(squaresize);
            Printsquare(squaresize, thesquare);
        }
        public static int FindSize()
        {
            Console.Write("What is the size of the square");
            int squaresize = Convert.ToInt16(Console.ReadLine());
            return (squaresize);
        }
        public static string[,] Makesquare(int squaresize)
        {
            string[,] thesquare = new string[squaresize, squaresize];
            bool checkside = true;
            for (int i = 0; i < squaresize; i++)
            {
                for (int j = 0; j < squaresize; j++)
                {
                    if (i == 0)
                    {
                        checkside = true;
                    }
                    if (j == 0)
                    {
                        checkside = true;
                    }
                    if (i == squaresize - 1)
                    {
                        checkside = true;
                    }
                    if (j == squaresize - 1)
                    {
                        checkside = true;
                    }
                    if (checkside == true)
                    {
                        thesquare[i, j] = "#";
                    }
                    else
                    {
                        thesquare[i, j] = " ";
                    }
                    checkside = false;
                }
            }
            return thesquare;
        }
        public static void Printsquare(int squaresize, string[,] thesquare)
        {
            Console.WriteLine("Printing Matrix: ");
            for (int k = 0; k < squaresize; k++)
            {
                for (int l = 0; l < squaresize; l++)
                {
                    Console.Write(thesquare[k, l]);
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}